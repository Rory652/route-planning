//
// Created by rory on 30/03/2021.
//

#ifndef ROUTE_PLANNING_TESTSEARCH_H
#define ROUTE_PLANNING_TESTSEARCH_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "load/load.h"
#include "search/search.h"
#include "unity.h"

// To create a dataset for later testing
void setUpTestFiles();

// Loads the dataset for testing
double **initialiseTest();

// Runs all the testing
void test_search();

#endif //ROUTE_PLANNING_TESTSEARCH_H
