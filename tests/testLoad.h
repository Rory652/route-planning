//
// Created by rory on 19/03/2021.
//

#ifndef ROUTE_PLANNING_TESTLOAD_H
#define ROUTE_PLANNING_TESTLOAD_H

#include <stdio.h>
#include "load/load.h"
#include "unity.h"

void test_loadFile();

void test_findLineByType();

void test_getValues();

#endif //ROUTE_PLANNING_TESTLOAD_H
