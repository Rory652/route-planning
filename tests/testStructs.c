//
// Created by rory on 02/04/2021.
//

#include "testStructs.h"

//Node related functions
void test_createNode() {
    // Valid Data
    TEST_ASSERT_EQUAL_INT(12345, createNode(12345, 53.805383, -1.556602).id);
    TEST_ASSERT_EQUAL_INT(87623, createNode(87623, 53.804329, -1.543764).id);

    //Boundary Data
    TEST_ASSERT_EQUAL_INT(12345, createNode(12345, minLat, minLong).id);
    TEST_ASSERT_EQUAL_INT(67890, createNode(67890, maxLat, maxLong).id);

    // Invalid Data
    //      All Invalid
    TEST_ASSERT_EQUAL_INT(0, createNode(26083241, 0, 0).id);

    //      1 Field Invalid
    TEST_ASSERT_EQUAL_INT(0, createNode(12345, 0, -1.5567).id);

}

void test_getNode() {
    if (numNodes == 0) {
        FILE *fPtr = fopen("../Final_Map.map", "r");
        loadFile(fPtr);
    }

    // Valid Data
    TEST_ASSERT_EQUAL_DOUBLE(53.803341,getNode(49509575).latitude);
    TEST_ASSERT_EQUAL_DOUBLE(-1.560578,getNode(920423484).longitude);

    // Invalid
    TEST_ASSERT_EQUAL_INT(-1, getNode(12345).id);
    TEST_ASSERT_EQUAL_INT(-1, getNode(1867901322).id);
}

void test_getNodePos() {
    if (numNodes == 0) {
        FILE *fPtr = fopen("../Final_Map.map", "r");
        loadFile(fPtr);
    }

    // Valid Data
    TEST_ASSERT_EQUAL_INT(5, getNodePos(1970536624));
    TEST_ASSERT_EQUAL_INT(1545, getNodePos(664212050));

    // Invalid
    TEST_ASSERT_EQUAL_INT(-1, getNodePos(-1887622579));
    TEST_ASSERT_EQUAL_INT(-1, getNodePos(12345678));
}

//Link related functions
void test_createLink() {
    // Valid Data
    TEST_ASSERT_EQUAL_INT(12345, createLink(12345, 100, 1000, 56.897).id);
    TEST_ASSERT_EQUAL_INT(898504, createLink(898504, 12983, 18324, 8.8743).id);

    // Invalid Data
    //      All Invalid
    TEST_ASSERT_EQUAL_INT(0, createLink(-2143395278, 0, -1887622548, -1.0).id);

    //      1 Field Invalid
    TEST_ASSERT_EQUAL_INT(0, createLink(67890, 123, 100, -1.0).id);
    TEST_ASSERT_EQUAL_INT(0, createLink(12345, 7, 3, -1.0).id);
}

void test_getLinks() {
    if (numLinks == 0) {
        FILE *fPtr = fopen("../Final_Map.map", "r");
        loadFile(fPtr);
    }

    // Valid Data
    TEST_ASSERT_EQUAL_INT(-2143392799, getLinks(53358700)[0].id);
    TEST_ASSERT_EQUAL_INT(-2143393512, getLinks(-2412)[1].id);

    // Invalid
    TEST_ASSERT_EQUAL_PTR(NULL, getLinks(12345));
    TEST_ASSERT_EQUAL_PTR(NULL, getLinks(-1887622579));
}

void test_findLink() {
    if (numLinks == 0) {
        FILE *fPtr = fopen("../Final_Map.map", "r");
        loadFile(fPtr);
    }

    // Valid Data
    TEST_ASSERT_EQUAL_INT(-2143393406, findLink(151917605, 152063520).id);
    TEST_ASSERT_EQUAL_INT(-2143393645, findLink(247958663, 247958662).id);

    // Boundary (Both valid nodes but no link between them)
    TEST_ASSERT_EQUAL_INT(-1, findLink(27081326, 247293217).id);

    // Invalid
    TEST_ASSERT_EQUAL_INT(-1, findLink(123763, 198302).id);
    TEST_ASSERT_EQUAL_INT(-1, findLink(948732, 29873982).id);
}
