//
// Created by rory on 02/04/2021.
//

#ifndef ROUTE_PLANNING_TESTSTRUCTS_H
#define ROUTE_PLANNING_TESTSTRUCTS_H

#include "structs/structs.h"
#include "load/load.h"
#include "unity.h"

//Node related functions
void test_createNode();
void test_getNode();
void test_getNodePos();

//Link related functions
void test_createLink();
void test_getLinks();
void test_findLink();

#endif //ROUTE_PLANNING_TESTSTRUCTS_H
