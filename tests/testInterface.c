//
// Created by rory on 31/03/2021.
//

#include "testInterface.h"

// Test strings are loaded in by the 'test_data.txt' file by rerouting stdin to that file
void test_inputString() {
    freopen("../tests/test_data.txt", "r", stdin);

    int n = 10;
    char test[n];

    // Valid string (not empty, not > n characters)
    inputString(test, n);
    TEST_ASSERT_EQUAL_STRING("test", test);

    // Empty String
    inputString(test, n);
    TEST_ASSERT_EQUAL_STRING("\n", test);

    // Null Pointer
    char *nullTemp = NULL;
    inputString(nullTemp, n);
    TEST_ASSERT_EQUAL_PTR(NULL, nullTemp);

    // String length > n
    inputString(test, n);
    TEST_ASSERT_EQUAL_STRING("StringMor", test);
}

void test_inputInt() {
    int test;

    // Valid Numbers
    test = inputInt();
    TEST_ASSERT_EQUAL_INT(76, test);

    test = inputInt();
    TEST_ASSERT_EQUAL_INT(1930, test);

    // Decimal Number
    test = inputInt();
    TEST_ASSERT_EQUAL_INT(7, test);

    // String before number
    test = inputInt();
    TEST_ASSERT_EQUAL_INT(0, test);

    // String after number
    test = inputInt();
    TEST_ASSERT_EQUAL_INT(123, test);

    // No number
    test = inputInt();
    TEST_ASSERT_EQUAL_INT(0, test);

    // Empty string
    test = inputInt();
    TEST_ASSERT_EQUAL_INT(0, test);

    // String too lone (> 50 characters)
    test = inputInt();
    TEST_ASSERT_EQUAL_INT(0, test);
}