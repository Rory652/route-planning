//
// Created by rory on 19/03/2021.
//

#include "testLoad.h"

void test_loadFile() {
    // Testing opening files
    FILE *fTest1 = fopen("notExists.txt", "r");
    TEST_ASSERT_EQUAL_INT(-1, loadFile(fTest1));
    // File doesn't exist
    FILE *fTest2 = fopen("../tests/badFile.txt", "r");
    TEST_ASSERT_EQUAL_INT(0, loadFile(fTest2));
    // File exists but doesn't follow formatting rules
    FILE *fTest3 = fopen("../Final_Map.map", "r");
    TEST_ASSERT_EQUAL_INT(1, loadFile(fTest3));

    // Testing some values taken from map file against what was loaded
    // Link 1000
    TEST_ASSERT_EQUAL_INT(-2143393758,allLinks[1000].id);
    // Node 2000
    TEST_ASSERT_EQUAL_DOUBLE(53.810149, allNodes[2000].latitude);
    // Node 3000
    TEST_ASSERT_EQUAL_DOUBLE(-1.551430, allNodes[1500].longitude);
}

void test_findLineByType() {
    char test[50];
    // NULL Pointer
    char *nullTest = NULL;
    TEST_ASSERT_EQUAL_INT(-1, findLineType(nullTest));
    // Blank Line
    strcpy(test, "");
    TEST_ASSERT_EQUAL_INT(-1, findLineType(test));
    // Invalid Line
    strcpy(test, "boundaries \\boundaries>");
    TEST_ASSERT_EQUAL_INT(-1, findLineType(test));
    // Valid Line 0 (bounding)
    strcpy(test, "bounding \\bounding>");
    TEST_ASSERT_EQUAL_INT(0, findLineType(test));
    // Valid Line 1 (link)
    strcpy(test, "link \\link>");
    TEST_ASSERT_EQUAL_INT(1, findLineType(test));
    // Valid Line 2 (node)
    strcpy(test, "node \\node>");
    TEST_ASSERT_EQUAL_INT(2, findLineType(test));
    // Valid Line 3 (way)
    strcpy(test, "way \\way>");
    TEST_ASSERT_EQUAL_INT(3, findLineType(test));
}

void test_getValues() {
    char test[100];
    double *returned;
    // NULL Line
    char *nullTest = NULL;
    returned = getValues(nullTest);
    TEST_ASSERT_EQUAL_PTR(NULL, returned);
    free(returned);
    // Empty Line
    strcpy(test, "");
    returned = getValues(test);
    TEST_ASSERT_EQUAL_PTR(NULL, returned);
    free(returned);
    // Invalid Format 1
    strcpy(test, "test test1=1.765283test2=4.12863 \\test>"); // Missing ' '
    returned = getValues(test);
    TEST_ASSERT_EQUAL_DOUBLE(1.765283, returned[0]);
    free(returned);
    // Invalid Format 2
    strcpy(test, "test test3.19872 \\test>"); // Missing '='
    returned = getValues(test);
    TEST_ASSERT_EQUAL_PTR(NULL, returned);
    free(returned);
    // 1 Value
    strcpy(test, "test test1=2.37634 \\test>");
    returned = getValues(test);
    TEST_ASSERT_EQUAL_DOUBLE(2.37634, returned[0]);
    free(returned);
    // 3 Values
    strcpy(test, "test test1=51.21863 test2=1.27638 test3=-18.187623 \\test>");
    returned = getValues(test);
    TEST_ASSERT_EQUAL_DOUBLE(-18.187623, returned[2]);
    free(returned);
}