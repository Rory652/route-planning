//
// Created by rory on 16/03/2021.
//
#include "unity.h"
#include "testLoad.h"
#include "testSearch.h"
#include "testInterface.h"
#include "testStructs.h"

void setUp() {}

void tearDown() {}

int main() {
    UNITY_BEGIN();
    // Load Module - loadFile
    RUN_TEST(test_loadFile);
    // Load Module - findLineType
    RUN_TEST(test_findLineByType);
    // Load Module - getValues
    RUN_TEST(test_getValues);

    // Search Module - dijkstraSearch
    RUN_TEST(test_search);

    // Interface Module - inputString
    RUN_TEST(test_inputString);
    // Interface Module - inputInt
    RUN_TEST(test_inputInt);

    // Structs Module - createNode
    RUN_TEST(test_createNode);
    // Structs Module - getNode
    RUN_TEST(test_getNode);
    // Structs Module - getNodePos
    RUN_TEST(test_getNodePos);
    // Structs Module - createLink
    RUN_TEST(test_createLink);
    // Structs Module - getLinks
    RUN_TEST(test_getLinks);
    // Structs Module - findLink
    RUN_TEST(test_findLink);

    return UNITY_END();
}
