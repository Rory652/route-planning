//
// Created by rory on 31/03/2021.
//

#ifndef ROUTE_PLANNING_TESTINTERFACE_H
#define ROUTE_PLANNING_TESTINTERFACE_H

#include "interface/interface.h"
#include "unity.h"
#include <stdio.h>
#include <stdlib.h>

void test_inputString();

void test_inputInt();

#endif //ROUTE_PLANNING_TESTINTERFACE_H
