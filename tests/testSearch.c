//
// Created by rory on 30/03/2021.
//

#include "testSearch.h"

// Runs every possible version of Dijkstra's algorithm for later tests
void setUpTestFiles() {
    // Loads the file if that hasn't already been done
    if (numNodes == 0) {
        FILE *fPtr = fopen("../Final_Map.map", "r");
        loadFile(fPtr);
    }

    // 'distances' is an array containing all the distances for a certain node, 'distance' is an individual node
    double *distances, distance;

    FILE *fWrite = fopen("../tests/search_data.txt", "w");

    // Goes through each node one at a time and writes all of the distances to each of the other nodes one at a time
    for (int i = 0; i < numNodes; i++) {
        distances = getDistances(i);
        for (int j = 0; j < numNodes; j++) {
            distance = distances[j];
            if (i == j) {
                distance = 0.0;
            } else if (distance == 10000000) {
                distance = -1.0;
            }

            // Ensures there is no dangling comma at the end of the line
            if (j != numNodes - 1) {
                fprintf(fWrite, "%f,", distance);
            } else {
                fprintf(fWrite, "%f", distance);
            }
        }
        free(distances);
        fprintf(fWrite, "\n");
    }

    fclose(fWrite);
}

// Loads in data for testing the search functions
double **initialiseTest() {
    FILE *fPtr = fopen("../tests/search_data.txt", "r");

    double **distances = (double **) malloc (sizeof(double *) * numNodes);

    if (fPtr != NULL) {
        // Some of the lines are massive so the seemingly excessive malloc is necessary
        char *line = (char *) malloc (sizeof(char) * 1000000), *current;
        int iPos = 0;

        // Reads file line by line, each line is all the distances for each node
        while (fscanf(fPtr, "%[^\n] ", line) != EOF) {
            distances[iPos] = (double *) malloc (sizeof(double) * numNodes);

            int jPos = 0;

            // Splits the line at each comma (each node)
            current = strtok(line, ",");

            while (current != NULL) {
                // Converts the number string into a double and saves it in an array
                distances[iPos][jPos] = strtod(current, NULL);

                jPos++;
                current = strtok(NULL, ",");
            }

            iPos++;
        }

        free(line);
        fclose(fPtr);
    }

    return distances;
}

void test_search() {
    // Seed the random number generator
    time_t t;
    srand((unsigned) time(&t));

    // Load nodes if they haven't already
    if (numNodes == 0) {
        FILE *fPtr = fopen("../Final_Map.map", "r");
        loadFile(fPtr);
    }
    double** testData = initialiseTest();

    // Test 10 different bits of data
    for (int i = 0; i < 50; i++) {
        int x = rand() % numNodes, y = rand() % numNodes;
        Route *testRoute = dijkstraSearch(x, y);

        TEST_ASSERT_EQUAL_DOUBLE(testData[x][y], getRouteLength(testRoute));
    }

    // Testing out of range data
    TEST_ASSERT_EQUAL_PTR(NULL, dijkstraSearch(0, -1));
    TEST_ASSERT_EQUAL_PTR(NULL, dijkstraSearch(-1, 0));
    TEST_ASSERT_EQUAL_PTR(NULL, dijkstraSearch(numNodes, numNodes + 7));
    TEST_ASSERT_EQUAL_PTR(NULL, dijkstraSearch(numNodes + 1, 1000));
    TEST_ASSERT_EQUAL_PTR(NULL, dijkstraSearch(-100, 10000));

}