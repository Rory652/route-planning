# Route Planning

# GitLab URL: 
https://gitlab.com/Rory652/route-planning

# Commit History
<details>
    <summary>Click for Full Commit History</summary>

_commit 8682b3843efcd1e583155ddc90ec80c32c087b02 (HEAD -> master, origin/master, origin/HEAD)_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Fri Apr 2 12:29:58 2021 +0100

    Fixed some issues in the CMake file to run the program on FengLinux


_commit 3bd16cdd3146975a279e9b65425218ef59ef635b_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Fri Apr 2 11:40:55 2021 +0100

    Wrote the tests for the structs module (which I had forgot to do). No issues came up in testings (all tests passed) so nothing had to be changed.

_commit f15474e288cad4e7684d8d68d862768231af960d_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Thu Apr 1 15:15:50 2021 +0100

    Fixed the issues with the interface module tests and got all of the tests to pass and fixed some problems that came up when compiling with flags to find warnings.

_commit e8b66968e73ee42ba15432b6d3e5b4a06e134eaf_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Wed Mar 31 15:58:33 2021 +0100

    Wrote the testing functions for the interface module, need to fix the bugs that came up with testing.

_commit ad048d36913d2fe3a0bf7c6499ffe8c97617e214_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Wed Mar 31 15:58:06 2021 +0100

    Finished writing tests for the search module, fixed a few bugs that appeared with different edge cases.

_commit 818e22d0c8590d4507860339928433c6015605a8_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Wed Mar 31 12:50:42 2021 +0100

    Tests written for the 'dikstraSearch' function ('minimumDist' tested in this function as well).

_commit f1d7be7105eafa537fa4783619ab9283a95ae2c6_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Tue Mar 30 17:14:26 2021 +0100

    Added quite a lot of comments to most of the code.

_commit d499bebec74e0b280ebdaf731d5a1517936d2a7b_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Tue Mar 30 14:06:39 2021 +0100

    Created the function to load in the massive array containing the data for different routes.

_commit aa99d1ed1917aa7deb2794c881ba2352f528e1d4_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Tue Mar 30 13:37:31 2021 +0100

    Created a new function in search to get the distances for every single possible route (~4 million) which was saved to a text file and can be used for testing.

_commit 0b1e7bec160c294d058983ee1fe76f0a970dbf67_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Mon Mar 29 11:10:11 2021 +0100

    Fixed the bug in the algorithm and allowed the user to input their own nodes for the route.

_commit 02f4ab2d9a250b0aea0384225a35cd753505dacd_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Sun Mar 28 17:47:20 2021 +0100

    Created the function for Dijkstra's Algorithm along with all other functions to get that to work. Need to let the users input their own nodes to find a route and fix an issue with the last node having an incorrect longitude value.

_commit 4c6dde3e59665f73561fe51f21775e4b7220d329_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Thu Mar 25 16:25:37 2021 +0000

    Made it so the 'getRouteLength' function works with the Route struct and not the link struct, as initially planned.

_commit 0d21820c38ef40f98c2011c4ecce6ced1aea92ba_
Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Thu Mar 25 16:24:40 2021 +0000

    Wrote the function to plot a specific route, also moved the command to close the plot from the plot function to the menu function (allows me to use 'plotGeneral' code in 'plotRoute'.

_commit 358da151ada67c15b74f18ee4979012da7f09e45_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Wed Mar 24 11:25:01 2021 +0000

    Wrote the basic functions of the interface, need to add support for finding custom routes.

_commit 1f213d38f354eb34fe336227511a6331a3a27ea7_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Wed Mar 24 11:18:28 2021 +0000

    Added colours to the graph and made it so the user has to press enter to close the graph (rather than it closing automatically after a few seconds)

_commit d458dfefc829b3761a87f133501d7a6612ebf694_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Wed Mar 24 11:10:21 2021 +0000

    Added a struct to store routes.

_commit f2b092513a1cab1b3ef2858b37e714179feadd6c_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Tue Mar 23 11:58:45 2021 +0000

    Adjusted the cmake file to use include directories properly.

_commit 12fbcfa30efc443af47f42991816f8cc176debf0_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Tue Mar 23 11:58:17 2021 +0000

    Wrote the code to plot the entire map and fixed some issues that come up with completed functions.

_commit 8e1de3672848cc79bd5ef5f8516bfe93edcfc8e1_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Fri Mar 19 15:11:06 2021 +0000

    Wrote and tested final tests I forgot about.

_commit edc304913945e60dbb495c17d0ab238bc87b8656_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Fri Mar 19 15:07:56 2021 +0000

    Tests for load module written and module is tested, all problems raised by tests have been fixed (mainly dealing with NULL/empty strings)

_commit 569adb208864f609867e1bfa0854f2696f88a52a_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Fri Mar 19 14:43:15 2021 +0000

    Finished the load Module, have to write tests next.

_commit 424640ba92de0acc0ae84759eeb1663d9d362733_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Thu Mar 18 17:02:25 2021 +0000

    Started 'load' module, each line can be read from file and decoded to find line type. Need to get values from each line next.

_commit 17106cddd254e33e7b182858c93e43a618d54df0_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Thu Mar 18 17:01:35 2021 +0000

    Finalised CMake changes to run from 1 file.

_commit e01d720bfcb69b2fd07ae08975455adaf486f611_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Thu Mar 18 13:14:57 2021 +0000

    Made it so both the program and tests from a single CMake file.

_commit e1fcc3a77261edd6a05f8c0290fc37d7d7e940b5_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Tue Mar 16 14:43:58 2021 +0000

    Reorganised some files and added Unity to allow for unit testing.

_commit 6b70788a5bc72fb301972ae85b453c24337aeb9e_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Mon Mar 15 19:30:21 2021 +0000

    Downloaded map file and outlined functions for each module. Also wrote basic functions for the struct module.

_commit 8491afe24936ef7dbf94f6a725742cf67074bed6_

Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Mon Mar 15 18:53:56 2021 +0000

    Created folders/files for all modules.

_commit 8add5a1e5faf9cfd127116e4ebc89057b8c39237_
Author: Rory652 <el20rdc@leeds.ac.uk>
Date:   Mon Mar 15 15:02:46 2021 +0000

    Created file structure and added library for working with GNUPLOT.

_commit 155b984eaac16dce0736849956a9fa14cf92f759_

Author: Rory Coe <el20rdc@leeds.ac.uk>
Date:   Thu Mar 11 12:32:30 2021 +0000

    Initial commit
</details>

# How to Run the Program
* Navigate to "route-planning/bin" folder in the terminal
  * 'cd path/to/route-planning/bin'
* Generate the build system
  * 'cmake ../'
* Build both the projects
  * 'cmake --build ./'
* Run the projects
  * Main Program - './route_planning'
  * Tests - './tests'