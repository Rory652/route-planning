//
// Created by rory on 15/03/2021.
//

#include "interface.h"

int menu() {
    system("clear");
    char *input = (char *) malloc(sizeof(char) * 50);

    while (1) {
        printf("Enter the option:\n");
        printf("\033[94m> Plot:\033[m\t\tPlot the entire map.\n");
        printf("\033[94m> Search:\033[m\tFind a route between 2 points.\n");
        printf("\033[94m> Quit:\033[m\t\tQuit the system.\n");

        inputString(input, 50);

        system("clear");
        if (strcasecmp(input, "plot") == 0) {
            gnuplot_ctrl *plot = plotGeneral();
            //Waits for user to press enter to close graph.
            printf("Press enter to close map.");
            getchar();
            gnuplot_close(plot);
        } else if (strcasecmp(input, "search") == 0) {
            int start, end, valid = 0;

            while (valid == 0) {
                printf("Enter starting node (1 - %i)\n> ", numNodes);
                start = inputInt() - 1;
                printf("Enter destination node (1 - %i)\n> ", numNodes);
                end = inputInt() - 1;

                if ((start >= 0 && start < numNodes) && (end >= 0 && end < numNodes)) {
                    valid = 1;
                } else {
                    system("clear");
                    printf("Error: Please enter a number between 1 and %i inclusive.\n", numNodes);
                }
            }

            Route *route = dijkstraSearch(start, end);

            if (route != NULL) {
                printf("The distance between %i and %i is %.2f.\nWould you like to plot this route (y/n)\n> ", start, end, getRouteLength(route));
                char ans = getchar();
                clearBuffer();
                if (ans == 'y' || ans =='Y') {
                    gnuplot_ctrl *plot = plotRoute(route);
                    //Waits for user to press enter to close graph.
                    printf("Press enter to close map.");
                    getchar();
                    gnuplot_close(plot);
                } else {
                    printf("Route wasn't plotted\n");
                }
            } else {
                printf("Error: There is no route from %i to %i\n", start, end);
            }

            // Freeing all memory allocated ot the route
            free(route->nodes);
            free(route->links);
            free(route);
        } else if (strcasecmp(input, "quit") == 0) {
            free(input);
            exit(1);
        } else {
            printf("\033[31mError:\033[m\tEnter 'plot', 'search' or 'quit'.\n");
        }

        system("clear");
    }

    return 1;
}

void inputString(char *string, int size) {
    if (string != NULL) {
        fgets(string, size, stdin);
        if ((int) strlen(string) == size - 1) {
            clearBuffer();
        }
        strtok(string, "\n");
    }
}

int inputInt() {
    char temp[50];
    int input;

    inputString(temp, 50);
    input = (int) strtol(temp, NULL, 10);

    return input;
}

void clearBuffer() {
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}