//
// Created by rory on 15/03/2021.
//

#ifndef ROUTE_PLANNING_INTERFACE_H
#define ROUTE_PLANNING_INTERFACE_H

#include <stdio.h>
#include <string.h>
#include "../structs/structs.h"
#include "../plot/plot.h"
#include "../search/search.h"

int menu();
//Inputs a string and stores it in pointer 'string'
void inputString(char *string, int size);
//Inputs an integer
int inputInt();
//Clears input buffer (prevents errors is entered string is too large)
void clearBuffer();

#endif //ROUTE_PLANNING_INTERFACE_H
