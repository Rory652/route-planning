//
// Created by rory on 15/03/2021.
//

#include "search.h"
#include <stdio.h>

// Decided not to do A* Search in the end, I was never planning on using it in the actual program.
// It serves no purpose and would be unnecessarily complicated (it is far more complex than Dijkstra's.

int minimumDist(int *included, double *distances) {
    double minDist = 1000000000000000;
    int position = -1;
    for (int i = 0; i < numNodes; i++) {
        if (included[i] == 0 && distances[i] < minDist) {
            minDist = distances[i];
            position = i;
        }
    }

    return position;
}

Route *dijkstraSearch(int start, int end) {
    //Checks that start and end values are in the allNodes array
    if ((start < numNodes && start >= 0) && (end < numNodes && end >= 0)) {
        double distances[numNodes]; // Holds distance from start node to all the other nodes
        int included[numNodes]; // Will be true if node at that integer
        int previousNode[numNodes]; // Tracks which node came before - so route can be found at the end of the search

        // Sets default value for arrays
        for (int i = 0; i < numNodes; i++) {
            distances[i] = 10000000;
            included[i] = 0;
            previousNode[i] = -1;
        }

        distances[start] = 0;

        // This ensures that it goes through every node (to guarantee shortest distance is found)
        for (int i = 0; i < numNodes - 1; i++) {
            int position = minimumDist(included, distances);
            int linkPos = 0;

            Link *linksToPos = getLinks(allNodes[position].id);

            // Goes through every link for each node
            while (linksToPos[linkPos].id != -1) {
                Link current = linksToPos[linkPos];
                int newNodePos;
                if (current.nodeId[0] != allNodes[position].id) {
                    newNodePos = getNodePos(current.nodeId[0]);
                } else {
                    newNodePos = getNodePos(current.nodeId[1]);
                }

                // Make sure links don't go back on themselves.
                if (included[newNodePos] != 1) {
                    if (distances[newNodePos] > distances[position] + current.distance) {
                        // Saves the new distance and the previous node (so route can be found later)
                        distances[newNodePos] = distances[position] + current.distance;
                        previousNode[newNodePos] = position;
                    }
                }

                linkPos++;
            }

            included[position] = 1;
            free(linksToPos);
        }

        Route *foundRoute = (Route *) malloc(sizeof(Route));
        foundRoute->length = 0;
        // Maximum memory is allocated (you don't know how long the route is so this avoids segmentation faults)
        foundRoute->nodes = (Node *) malloc (sizeof(Node) * numNodes);
        foundRoute->links = (Link *) malloc (sizeof(Link) * numLinks);
        if (included[end] == 1) {
            int current = end, temp;
            while (current != -1) {
                foundRoute->nodes[foundRoute->length] = allNodes[current];
                temp = current;
                current = previousNode[temp];
                if (current != -1) {
                    // Finds the link between the next 2 nodes in the list and saves that
                    Link tempLink = findLink(allNodes[temp].id, allNodes[current].id);
                    if (tempLink.id != -1) {
                        foundRoute->links[foundRoute->length] = tempLink;
                    } else {
                        // Error: no link found between 2 nodes
                        return NULL;
                    }
                    foundRoute->length++;
                } else if (current == -1 && temp != start) {
                    return NULL;
                }
            }
            foundRoute->nodes[foundRoute->length] = allNodes[start];
            return foundRoute;
        } else {
            return NULL;
        }
    }
    return NULL;
}

double *getDistances(int start) {
    //Checks that start and end values are in the allNodes array
    if (start < numNodes && start >= 0) {
        double *distances = (double *) malloc (sizeof(double) * numNodes); // Holds distance from start node to all the other nodes
        int included[numNodes]; // Will be true if node at that integer

        //Sets default value for arrays
        for (int i = 0; i < numNodes; i++) {
            distances[i] = 10000000;
            included[i] = 0;
        }

        distances[start] = 0;

        for (int i = 0; i < numNodes - 1; i++) {
            int position = minimumDist(included, distances);
            int linkPos = 0;

            Link *linksToPos = getLinks(allNodes[position].id);

            while (linksToPos[linkPos].id != -1) {
                Link current = linksToPos[linkPos];
                int newNodePos;
                if (current.nodeId[0] != allNodes[position].id) {
                    newNodePos = getNodePos(current.nodeId[0]);
                } else {
                    newNodePos = getNodePos(current.nodeId[1]);
                }

                // Make sure links don't go back on themselves.
                if (included[newNodePos] != 1) {
                    if (distances[newNodePos] > distances[position] + current.distance) {
                        distances[newNodePos] = distances[position] + current.distance;
                    }
                }

                linkPos++;
            }

            included[position] = 1;
            free(linksToPos);
        }
        return distances;
    }

    return NULL;
}
