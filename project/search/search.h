//
// Created by rory on 15/03/2021.
//

#ifndef ROUTE_PLANNING_SEARCH_H
#define ROUTE_PLANNING_SEARCH_H

#include "../structs/structs.h"

//Function to
Route *dijkstraSearch(int start, int end);

double *getDistances(int start);

// Finds minimum distance not in included array
int minimumDist(int *included, double *distances);

#endif //ROUTE_PLANNING_SEARCH_H
