//
// Created by rory on 15/03/2021.
//

#ifndef ROUTE_PLANNING_STRUCTS_H
#define ROUTE_PLANNING_STRUCTS_H

#include <stdlib.h>
#include "../sysValues.h"

//Structure for Nodes
typedef struct Node {
    int id;
    double latitude;
    double longitude;
} Node;

//Array for all nodes.
Node *allNodes;
extern int numNodes;

//Structure for Links
typedef struct Link {
    int id;
    int nodeId[2];
    double distance;
} Link;

//Array for all links
Link *allLinks;
extern int numLinks;

//Structure to store a Route
typedef struct Route {
    int length;
    Link *links;
    Node *nodes;
} Route;

//Node related functions
Node createNode(int id, double lat, double lon);
int checkNodeId(int id);
Node getNode(int id);
int getNodePos(int id); // Gets position of node in allNodes array from its id

//Link related functions
Link createLink(int id, int node1, int node2, double dist);
int checkLinkId(int id);
Link *getLinks(int nodeId);
Link findLink (int node1, int node2);
double getRouteLength(Route *route);

#endif //ROUTE_PLANNING_STRUCTS_H
