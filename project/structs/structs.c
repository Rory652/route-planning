//
// Created by rory on 15/03/2021.
//

#include "structs.h"

int numNodes = 0;
int numLinks = 0;

// Creates a new node struct
Node createNode(int id, double lat, double lon) {
    Node new;

    if (checkNodeId(id) == 1 && (lat >= minLat && lat <= maxLat) && (lon >= minLong && lon <= maxLong)) {
        new.id = id;
        new.latitude = lat;
        new.longitude = lon;
    } else {
        new.id = 0;
    }

    return new;
}

// Checks that a node id is valid (unique)
int checkNodeId(int id) {
    for (int i = 0; i < numNodes; i++) {
        if (id == allNodes[i].id) {
            return 0;
        }
    }
    return 1;
}

//Returns a node from it's ID, to get information about positions from Link
Node getNode(int id) {
    for (int i = 0; i < numNodes; i++) {
        if (allNodes[i].id == id) {
            return allNodes[i];
        }
    }

    Node error;
    error.id = -1;
    return error;
}

// Finds position of a node in the 'allNodes' array
int getNodePos(int id) {
    for (int i = 0; i < numNodes; i++) {
        if (allNodes[i].id == id) {
            return i;
        }
    }
    return -1;
}

// Creates a new link struct
Link createLink(int id, int node1, int node2, double dist) {
    Link new;

    if (checkLinkId(id) == 1 && node1 != 0 && node2 != 0 && dist > 0.0) {
        new.id = id;
        new.nodeId[0]= node1;
        new.nodeId[1]= node2;
        new.distance = dist;
    } else {
        new.id = 0;
    }

    return new;
}

// Checks if a link if is valid (unique)
int checkLinkId(int id) {
    for (int i = 0; i < numLinks; i++) {
        if (id == allLinks[i].id) {
            return 0;
        }
    }
    return 1;
}

// Finds every link for a certain node and returns a pointer to them
Link *getLinks(int nodeId) {
    Link *found = (Link *) malloc(sizeof(Link) * numLinks);
    int size = 0;
    for (int i = 0; i < numLinks; i++) {
        if (allLinks[i].nodeId[0] == nodeId || allLinks[i].nodeId[1] == nodeId) {
            found[size] = allLinks[i];
            size++;
        }
    }
    if (size == 0) {
        found = NULL;
    } else {
        Link end;
        end.id = -1;
        found[size] = end;
        size++;
    }
    return found;
}

// Finds a link between 2 nodes
Link findLink (int node1, int node2) {
    for (int i = 0; i < numLinks; i++) {
        if ((allLinks[i].nodeId[0] == node1 && allLinks[i].nodeId[1] == node2) ||
                (allLinks[i].nodeId[1] == node1 && allLinks[i].nodeId[0] == node2)) {
            return allLinks[i];
        }
    }

    Link notFound;
    notFound.id = -1;
    return notFound;
}

// Finds the total distance in a route by looking at all of the links
double getRouteLength(Route *route) {
    if (route != NULL) {
        double total = 0.0;
        for (int i = 0; i < route->length; i++) {
            total += route->links[i].distance;
        }
        return total;
    } else {
        return -1.0;
    }
}