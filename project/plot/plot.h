//
// Created by rory on 15/03/2021.
//

#ifndef ROUTE_PLANNING_PLOT_H
#define ROUTE_PLANNING_PLOT_H

#include "gnuplot_i.h"
#include "../structs/structs.h"

//Plots all routes
gnuplot_ctrl *plotGeneral();
//Plots all routes but highlights a specific route in another colour
gnuplot_ctrl *plotRoute(Route *route);

#endif //ROUTE_PLANNING_PLOT_H
