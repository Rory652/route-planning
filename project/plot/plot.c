//
// Created by rory on 15/03/2021.
//

#include "plot.h"

// Plots every node/link in the allNodes/allLinks arrays
gnuplot_ctrl *plotGeneral() {
    gnuplot_ctrl *map;
    map = gnuplot_init();

    // Sets the default range of the graph
    gnuplot_cmd(map, "set xrange[-1.565:-1.54]");
    gnuplot_cmd(map, "set yrange[53.801:53.812]");

    double x[numNodes];
    double y[numNodes];
    // Gets the latitude and longitude coordinates for each node
    for (int i = 0; i < numNodes; i++) {
        x[i] = allNodes[i].longitude;
        y[i] = allNodes[i].latitude;
    }

    //Plot paths
    for (int i = 0; i < numLinks; i++) {
        Node start = getNode(allLinks[i].nodeId[0]), end = getNode(allLinks[i].nodeId[1]);
        gnuplot_cmd(map, "set arrow from %f,%f to %f,%f nohead lc rgb 'blue'", start.longitude, start.latitude, end.longitude, end.latitude);
    }

    // Actual command to plot the nodes
    gnuplot_plot_xy(map, x, y, numNodes, "Map");

    //Changes the colour of the graph
    gnuplot_cmd(map, "set linetype 1 lc rgb 'blue'");
    gnuplot_cmd(map, "replot");

    return map;
}

gnuplot_ctrl *plotRoute(Route *route) {
    // Plots the base of the map, so route can be put on top of it.
    gnuplot_ctrl *base = plotGeneral();

    // To store all nodes in the route (so they can be plotted in a different colour)
    double x[route->length + 1];
    double y[route->length + 1];

    // Gets the coordinates for each of the nodes in the route
    for (int i = 0; i < route->length; i++) {
        x[i] = route->nodes[i].longitude;
        y[i] = route->nodes[i].latitude;

        // Add in the last node
        if (i == route->length - 1) {
            x[i + 1] = route->nodes[i + 1].longitude;
            y[i + 1] = route->nodes[i + 1].latitude;
        }

        // Plots the new links
        Node start = getNode(route->links[i].nodeId[0]), end = getNode(route->links[i].nodeId[1]);
        gnuplot_cmd(base, "set arrow from %f,%f to %f,%f nohead lc rgb 'red'", start.longitude, start.latitude, end.longitude, end.latitude);
    }

    // Plots the new nodes
    gnuplot_plot_xy(base, x, y, route->length + 1, "Route");

    //Sets colour of the new plot
    gnuplot_cmd(base, "set linetype 2 lc rgb 'red'");
    gnuplot_cmd(base, "replot");

    return base;
}
