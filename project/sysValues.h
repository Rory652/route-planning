//
// Created by rory on 23/03/2021.
//

#ifndef ROUTE_PLANNING_SYSVALUES_H
#define ROUTE_PLANNING_SYSVALUES_H

//Define the bounding of the grid
double minLat, minLong, maxLat, maxLong;

#endif //ROUTE_PLANNING_SYSVALUES_H
