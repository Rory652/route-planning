//
// Created by rory on 15/03/2021.
//

#ifndef ROUTE_PLANNING_LOAD_H
#define ROUTE_PLANNING_LOAD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../sysValues.h"
#include "../structs/structs.h"

// Master function for this module
int loadFile(FILE *fPtr);
int findLineType(char* line);
double *getValues(char* line);

#endif //ROUTE_PLANNING_LOAD_H
