//
// Created by rory on 15/03/2021.
//

#include "load.h"

int loadFile(FILE *fPtr) {
    int lineType = -2;
    char *line = (char *) malloc(sizeof(char) * 1000);
    double *tempValues;
    if (fPtr != NULL) {
        // Reads file line by line until an invalid line type is found
        while (fscanf(fPtr, "<%[^\n] ", line) != EOF && lineType != -1 && lineType != 3) {
            lineType = findLineType(line);
            if (lineType != -1) {
                tempValues = getValues(line);
                if (tempValues != NULL) {
                    // Finds out what type of line it was and then creates the appropriate variable
                    if (lineType == 0) {
                        minLat = tempValues[0];
                        minLong = tempValues[1];
                        maxLat = tempValues[2];
                        maxLong = tempValues[3];
                    } else if (lineType == 1) {
                        Link newLink = createLink((int) tempValues[0], (int) tempValues[1],
                                                  (int) tempValues[2], tempValues[4]);
                        if (newLink.id != 0) {
                            numLinks++;
                            allLinks = realloc(allLinks, sizeof(Link) * numLinks);
                            allLinks[numLinks - 1] = newLink;
                        }
                    } else if (lineType == 2) {
                        Node newNode = createNode((int) tempValues[0], tempValues[1], tempValues[2]);
                        if (newNode.id != 0) {
                            Link *links = getLinks(newNode.id);
                            if (links != NULL) {
                                numNodes++;
                                allNodes = realloc(allNodes, sizeof(Node) * numNodes);
                                allNodes[numNodes - 1] = newNode;
                            }
                            free(links);
                        }
                    }
                } else {
                    return -1; // Error: Set of values returned as null
                }

                free(tempValues);
            }
        }

        free(line);
        fclose(fPtr);

        if (lineType == 3) {
            // File was correctly loaded
            return 1;
        }

        // Error: file formatting is invalid
        return 0;
    }
    free(line);
    // Error: file doesn't exist
    return -1;
}

// Finds what type of line the line is
int findLineType(char* line){
    if (line == NULL || strlen(line) == 0) {
        return -1;
    }

    char *lineCpy = (char *) malloc (sizeof(char) * (strlen(line) + 1));
    int toReturn;
    strcpy(lineCpy, line); // Allows me to manipulate the string without damaging the original
    lineCpy = strtok(lineCpy, " ");
    if (strcasecmp(lineCpy, "bounding") == 0) {
        toReturn = 0;
    } else if (strcasecmp(lineCpy, "link") == 0) {
        toReturn = 1;
    } else if (strcasecmp(lineCpy, "node") == 0) {
        toReturn = 2;
    } else if (strcasecmp(lineCpy, "way") == 0) {
        toReturn = 3; // Signifies the end of the file if formatted correctly
    } else {
        toReturn = -1;
    }
    free(lineCpy);
    return toReturn;
}

// Turns a line (in string form) into an array of values to be used later
double *getValues(char* line) {
    if (line == NULL || strlen(line) == 0) {
        return NULL;
    }

    double *values = NULL;
    // Allows me to find the start and end of each number
    char startNum = '=', endNum = ' ', endLine = '\\', current = ' ', temp[50], *endPtr;
    int pos = 0, inNum = 0, arraySize = 0, subPos = 0;

    while (current != '\0') {
        current = line[pos];

        // Keeps track of the start of the line (whether or not it's currently looking at a number, etc.)
        if (current == startNum && inNum == 0) {
            inNum = 1;
        } else if (inNum == 1 && current != endNum) {
            temp[subPos] = current;
            subPos++;
        } else if (inNum == 1 && current == endNum) {
            arraySize++;
            temp[subPos] = '\0';
            values = realloc(values, sizeof(double) * arraySize);
            values[arraySize - 1] = strtod(temp, &endPtr); // Converts string of numbers into a double and stores it.
            // If there is an error in the number string, a 0 is returned and can be used to check for errors.

            subPos = 0;
            inNum = 0;
        }

        if (current == endLine) {
            return values;
        }

        pos++;
    }

    return values;
}
